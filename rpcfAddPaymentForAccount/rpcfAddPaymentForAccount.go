package rpcfAddPaymentForAccount

import (
	"fmt"
	"time"

	"bitbucket.org/chubi/urfago"
)

type rpcfAddPaymentForAccount struct {
	AccountId        int32
	Unused           int32
	Payment          float64
	CurrencyId       int32
	PaymentDate      time.Time
	BurnDate         time.Time
	PaymentMethod    int32
	AdminComment     string
	Comment          string
	PaymentExtNumber string
	PaymentToInvoice int32
	TurnOnInet       int32
}

type rpcfAddPaymentForAccountRes struct {
	PaymentTransactionId int32
}

func New() (req *rpcfAddPaymentForAccount) {
	req = &rpcfAddPaymentForAccount{}
	req.Unused = 0
	req.CurrencyId = 810
	req.PaymentDate = time.Now()
	req.BurnDate = time.Unix(0, 0)
	req.PaymentMethod = 1
	req.TurnOnInet = 1
	return
}

func (r *rpcfAddPaymentForAccount) Run(u *urfago.Urfa) (res *rpcfAddPaymentForAccountRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x3110)
	if err != nil {
		return
	}
	packet := u.GetPacket()
	packet.AddData(r.AccountId)
	packet.AddData(r.Unused)
	packet.AddData(r.Payment)
	packet.AddData(r.CurrencyId)
	packet.AddData(r.PaymentDate)
	packet.AddData(r.BurnDate)
	packet.AddData(r.PaymentMethod)
	packet.AddData(r.AdminComment)
	packet.AddData(r.Comment)
	packet.AddData(r.PaymentExtNumber)
	packet.AddData(r.PaymentToInvoice)
	packet.AddData(r.TurnOnInet)
	u.SendParam()

	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()

	res = &rpcfAddPaymentForAccountRes{}
	reader.Get(&res.PaymentTransactionId)
	return
}
