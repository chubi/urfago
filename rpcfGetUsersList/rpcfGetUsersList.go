package rpcfGetUsersList

import (
	"fmt"
	"net"

	"bitbucket.org/chubi/urfago"
)

type User struct {
	UserId        int32
	Login         string
	BasicAccount  int32
	FullName      string
	IsBlocked     int32
	Balance       float64
	IpAdrSize     int32
	IpGroups      []IpGroup
	UserIntStatus int32
}

type Ip struct {
	IpAddress net.IP
	Mask      net.IP
	GroupType int32
}

type IpGroup struct {
	GroupSize int32
	Ips       []Ip
}

type rpcfGetUsersList struct {
	From     int32
	To       int32
	CardUser int32
}

type rpcfGetUsersListRes struct {
	Cnt   int32
	Users []User
}

func New() (req *rpcfGetUsersList) {
	req = &rpcfGetUsersList{}
	req.CardUser = 0
	return
}

func (r *rpcfGetUsersList) Run(u *urfago.Urfa) (res *rpcfGetUsersListRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x2001)
	if err != nil {
		return
	}
	packet := u.GetPacket()
	packet.AddData(r.From)
	packet.AddData(r.To)
	packet.AddData(r.CardUser)

	u.SendParam()

	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()

	res = &rpcfGetUsersListRes{}
	reader.Get(&res.Cnt)
	for i := int32(0); i < res.Cnt; i++ {
		user := User{}
		reader.Get(&user.UserId)
		reader.Get(&user.Login)
		reader.Get(&user.BasicAccount)
		reader.Get(&user.FullName)
		reader.Get(&user.IsBlocked)
		reader.Get(&user.Balance)
		reader.Get(&user.IpAdrSize)
		for j := int32(0); j < user.IpAdrSize; j++ {
			group := IpGroup{}
			reader.Get(&group.GroupSize)
			for x := int32(0); x < group.GroupSize; x++ {
				ip := Ip{}
				reader.Get(&ip.IpAddress)
				reader.Get(&ip.Mask)
				reader.Get(&ip.GroupType)
				group.Ips = append(group.Ips, ip)
			}
			user.IpGroups = append(user.IpGroups, group)
		}
		reader.Get(&user.UserIntStatus)
		res.Users = append(res.Users, user)
	}
	return
}
