package urfago

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"sync"

	"github.com/spacemonkeygo/openssl"
)

type Urfa struct {
	mu       *sync.Mutex
	conn     net.Conn
	reader   *bufio.Reader
	writer   *bufio.Writer
	packet   *packet
	username string
	password string
	address  string
}

func NewUrfa(address, username, password string) (urfa *Urfa, err error) {
	urfa = &Urfa{username: username, password: password, address: address}
	urfa.mu = new(sync.Mutex)
	err = urfa.connect()
	return
}

func (u *Urfa) Ping() bool {
	u.packet.clean()
	u.packet.code = 201
	u.packet.setAttrInt(3, 0x00001)
	if err := u.sendPacket(); err != nil {
		return false
	}
	if err := u.readPacket(); err != nil {
		return false
	}
	attr := u.packet.getAttr(4)
	if attr == nil {
		return false
	}
	return true
}

func (u *Urfa) PingWithReconnect() (ok bool, err error) {
	ok = u.Ping()
	if ok {
		return
	}
	if err = u.reconnect(); err != nil {
		return
	}
	ok = true
	return
}

func (u *Urfa) Close() {
	if u.conn != nil {
		u.conn.Close()
	}
	return
}

func (u *Urfa) Lock() {
	u.mu.Lock()
}

func (u *Urfa) Unlock() {
	u.mu.Unlock()
}

func (u *Urfa) connect() (err error) {
	if u.conn, err = net.Dial("tcp", u.address); err != nil {
		return
	}
	u.reader = bufio.NewReader(u.conn)
	u.writer = bufio.NewWriter(u.conn)
	u.packet = &packet{}
	if err = u.login(); err != nil {
		return
	}
	return
}

func (u *Urfa) login() (err error) {
	for {
		if err = u.readPacket(); err != nil {
			return
		}
		if u.packet.code == 192 {
			digest := u.packet.attr[6].data
			u.packet.clean()
			u.packet.code = 193
			hash := md5.New()
			io.WriteString(hash, string(digest))
			io.WriteString(hash, u.password)
			u.packet.setAttrString(2, u.username)
			u.packet.setAttrString(8, string(digest))
			u.packet.setAttrString(9, string(hash.Sum(nil)))
			u.packet.setAttrInt(10, 4)
			u.packet.setAttrInt(1, 2)
			u.sendPacket()
		}
		if u.packet.code == 194 {
			var ctx *openssl.Ctx
			ctx, err = sslCtx()
			if u.conn, err = openssl.Client(u.conn, ctx); err != nil {
				return
			}
			u.reader = bufio.NewReader(u.conn)
			u.writer = bufio.NewWriter(u.conn)
			return
		}
		if u.packet.code == 195 {
			return fmt.Errorf("Wrong auth")
		}
	}
}

func (u *Urfa) reconnect() (err error) {
	u.Close()
	err = u.connect()
	return
}

func (u *Urfa) readPacket() (err error) {
	u.packet.clean()
	if err = binary.Read(u.reader, binary.BigEndian, &u.packet.code); err != nil {
		return
	}
	if err = binary.Read(u.reader, binary.BigEndian, &u.packet.version); err != nil {
		return
	}
	if err = binary.Read(u.reader, binary.BigEndian, &u.packet.len); err != nil {
		return
	}
	tmplen := uint16(4)
	var code int16
	var lenght uint16
	for tmplen < u.packet.len {
		if err = binary.Read(u.reader, binary.LittleEndian, &code); err != nil {
			return
		}
		if err = binary.Read(u.reader, binary.BigEndian, &lenght); err != nil {
			return
		}
		tmplen += lenght
		if code == 5 {
			data := make([]byte, lenght-4)
			if _, err = u.reader.Read(data); err != nil {
				return
			}
			u.packet.data = append(u.packet.data, data)
		} else {
			u.packet.attr[code] = attr{make([]byte, lenght-4), lenght}
			if _, err = u.reader.Read(u.packet.attr[code].data); err != nil {
				return
			}
		}
	}
	return
}

func (u *Urfa) sendPacket() (err error) {
	buf := new(bytes.Buffer)
	buf.WriteByte(byte(u.packet.code))
	buf.WriteByte(byte(u.packet.version))
	binary.Write(buf, binary.BigEndian, u.packet.len)
	for code, attr := range u.packet.attr {
		binary.Write(buf, binary.LittleEndian, uint16(code))
		binary.Write(buf, binary.BigEndian, attr.len)
		buf.Write(attr.data)
	}
	for _, d := range u.packet.data {
		binary.Write(buf, binary.LittleEndian, uint16(5))
		binary.Write(buf, binary.BigEndian, uint16(len(d)+4))
		buf.Write(d)
	}
	_, err = u.writer.Write(buf.Bytes())
	u.writer.Flush()
	return
}

func (u *Urfa) Call(code int32) (err error) {
	u.packet.clean()
	u.packet.code = 201
	u.packet.setAttrInt(3, code)
	if err = u.sendPacket(); err != nil {
		return
	}
	if err = u.readPacket(); err != nil {
		return
	}
	attr := u.packet.getAttr(4)
	if attr != nil {
		err = fmt.Errorf("Call func 0x%x err: %s", code, attr.data)
		return
	}
	return
}

func (u *Urfa) SendParam() (err error) {
	u.packet.code = 200
	err = u.sendPacket()
	return
}

func (u *Urfa) GetPacket() *packet {
	return u.packet
}
