package rpcfPaymentsReportNew

import (
	"fmt"
	"time"

	"bitbucket.org/chubi/urfago"
)

type Attr struct {
	Id                int32
	AccountId         int32
	Login             string
	ActualDate        time.Time
	PaymentEnterDate  time.Time
	Payment           float64
	PaymentIncurrency float64
	CurrencyId        int32
	Method            int32
	WhoReceved        int32
	AdminComment      string
	PaymentExtNumber  string
	FullName          string
	AccExternalId     string
	BurntDate         time.Time
}

type rpcfPaymentsReportNew struct {
	UserId    int32
	AccountId int32
	GroupId   int32
	Apid      int32
	TimeStart time.Time
	TimeEnd   time.Time
}

type rpcfPaymentsReportNewRes struct {
	UsersCount int32
	AttrCount  int32
	Attrs      []Attr
}

func New() (res *rpcfPaymentsReportNew) {
	res = &rpcfPaymentsReportNew{}
	res.TimeEnd = time.Now()
	return
}

func (r *rpcfPaymentsReportNew) Run(u *urfago.Urfa) (res *rpcfPaymentsReportNewRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x3030)
	if err != nil {
		return
	}
	packet := u.GetPacket()
	packet.AddData(r.UserId)
	packet.AddData(r.AccountId)
	packet.AddData(r.GroupId)
	packet.AddData(r.Apid)
	packet.AddData(r.TimeStart)
	packet.AddData(r.TimeEnd)
	u.SendParam()

	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()
	res = &rpcfPaymentsReportNewRes{}
	reader.Get(&res.UsersCount)
	for i := int32(0); i < res.UsersCount; i++ {
		reader.Get(&res.AttrCount)
		for j := int32(0); j < res.AttrCount; j++ {
			attr := Attr{}
			reader.Get(&attr.Id)
			reader.Get(&attr.AccountId)
			reader.Get(&attr.Login)
			reader.Get(&attr.ActualDate)
			reader.Get(&attr.PaymentEnterDate)
			reader.Get(&attr.Payment)
			reader.Get(&attr.PaymentIncurrency)
			reader.Get(&attr.CurrencyId)
			reader.Get(&attr.Method)
			reader.Get(&attr.WhoReceved)
			reader.Get(&attr.AdminComment)
			reader.Get(&attr.PaymentExtNumber)
			reader.Get(&attr.FullName)
			reader.Get(&attr.AccExternalId)
			reader.Get(&attr.BurntDate)
			res.Attrs = append(res.Attrs, attr)
		}
	}
	return
}
