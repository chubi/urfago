package rpcfGetUserAccountList

import (
	"fmt"

	"bitbucket.org/chubi/urfago"
)

type rpcfGetUserAccountList struct {
	UserId int32
}

type Account struct {
	Account     int32
	AccountName string
}

type rpcfGetUserAccountListRes struct {
	AccountsCount int32
	Accounts      []Account
}

func New() (req *rpcfGetUserAccountList) {
	req = &rpcfGetUserAccountList{}
	return
}

func (r *rpcfGetUserAccountList) Run(u *urfago.Urfa) (res *rpcfGetUserAccountListRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x2033)
	if err != nil {
		return
	}

	packet := u.GetPacket()
	packet.AddData(r.UserId)
	u.SendParam()
	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("User with id %d not found", r.UserId)
		return
	}
	defer reader.Clean()
	res = &rpcfGetUserAccountListRes{}
	reader.Get(&res.AccountsCount)
	for i := int32(0); i < res.AccountsCount; i++ {
		account := Account{}
		reader.Get(&account.Account)
		reader.Get(&account.AccountName)
		res.Accounts = append(res.Accounts, account)
	}
	return
}
