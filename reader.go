package urfago

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"time"
)

type Reader struct {
	urfa *Urfa
	iter int32
}

func NewReader(u *Urfa) (reader *Reader, err error) {
	u.packet.clean()
	reader = &Reader{u, 0}
	if err = reader.getpacket(); err != nil {
		return
	}
	return
}

func (r *Reader) getpacket() (err error) {
	if r.iter >= int32(len(r.urfa.packet.data)) {
		r.iter = 0
		err = r.urfa.readPacket()
		if err != nil {
			return
		}
		if r.urfa.packet.getAttr(4) != nil {
			return fmt.Errorf("Data done")
		}
	}
	return
}

func (r *Reader) Clean() {
	for {
		r.urfa.readPacket()
		if r.urfa.packet.getAttr(4) != nil {
			break
		}
	}
}

func (r *Reader) Get(v interface{}) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	switch v := v.(type) {
	case *int32:
		buf := bytes.NewReader(r.urfa.packet.data[r.iter])
		binary.Read(buf, binary.BigEndian, v)
	case *float64:
		buf := bytes.NewReader(r.urfa.packet.data[r.iter])
		binary.Read(buf, binary.BigEndian, v)
	case *net.IP:
		*v = net.IP{r.urfa.packet.data[r.iter][0], r.urfa.packet.data[r.iter][1],
			r.urfa.packet.data[r.iter][2], r.urfa.packet.data[r.iter][3]}
	case *string:
		*v = string(r.urfa.packet.data[r.iter])
	case *time.Time:
		var intTime int32
		buf := bytes.NewReader(r.urfa.packet.data[r.iter])
		binary.Read(buf, binary.BigEndian, &intTime)
		*v = time.Unix(int64(intTime), 0)
	default:
		log.Printf("SSS. Wrong type %t!!!!!", v)
		err = fmt.Errorf("SSS. Wrong type %t!!!!!", v)
	}
	r.iter++
	return
}
