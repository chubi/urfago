package rpcfGetHousesList

import (
	"fmt"
	"time"

	"bitbucket.org/chubi/urfago"
)

type House struct {
	HouseId     int32
	IpZoneId    int32
	ConnectDate time.Time
	PostCode    string
	Country     string
	Region      string
	City        string
	Street      string
	Number      string
	Building    string
}

type rpcfGetHousesListRes struct {
	HousesSize int32
	Houses     []House
}
type rpcfGetHousesList struct{}

func New() (rpc *rpcfGetHousesList) {
	rpc = &rpcfGetHousesList{}
	return
}

func (r *rpcfGetHousesList) Run(u *urfago.Urfa) (res *rpcfGetHousesListRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x2810)
	if err != nil {
		return
	}
	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()

	res = &rpcfGetHousesListRes{}
	reader.Get(&res.HousesSize)
	for i := int32(0); i < res.HousesSize; i++ {
		house := House{}
		reader.Get(&house.HouseId)
		reader.Get(&house.IpZoneId)
		reader.Get(&house.ConnectDate)
		reader.Get(&house.PostCode)
		reader.Get(&house.Country)
		reader.Get(&house.Region)
		reader.Get(&house.City)
		reader.Get(&house.Street)
		reader.Get(&house.Number)
		reader.Get(&house.Building)
		res.Houses = append(res.Houses, house)
	}
	return
}
