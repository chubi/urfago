package urfago

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"time"
)

type attr struct {
	data []byte
	len  uint16
}

type packet struct {
	code    uint8
	version uint8
	len     uint16
	attr    map[int16]attr
	data    [][]byte
}

func newPacket() *packet {
	p := &packet{}
	p.code = 0
	p.version = 35
	p.len = 4
	p.attr = make(map[int16]attr)
	p.data = [][]byte{}
	return p
}

func (p *packet) clean() {
	p.code = 0
	p.version = 35
	p.len = 4
	p.attr = make(map[int16]attr)
	p.data = [][]byte{}
}

func (p *packet) dump() {
	fmt.Printf("Packet dump:\n\tCode: %d\n\tVersion: %d\n\tLen: %d\n", p.code, p.version, p.len)
	fmt.Printf("\tAttributes:\n")
	for key, value := range p.attr {
		fmt.Printf("\t\tAttr %d: len => %d, data => %v\n", key, value.len, value.data)
	}
	fmt.Printf("\tData:\n")
	for key, value := range p.data {
		fmt.Printf("\t\t%d => %v\n", key, value)
	}
}

func (p *packet) getAttr(attr int16) *attr {
	a, ok := p.attr[attr]
	if ok {
		return &a
	}
	return nil
}

func (p *packet) setAttrString(code int16, data string) {
	buf := new(bytes.Buffer)
	buf.WriteString(data)
	p.attr[code] = attr{buf.Bytes(), uint16(len(data) + 4)}
	p.len += uint16((len(data) + 4))
}

func (p *packet) setAttrInt(code int16, data int32) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, data)
	p.attr[code] = attr{buf.Bytes(), 8}
	p.len += 8
}

func (p *packet) AddDataInt(data int32) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, data)
	p.data = append(p.data, buf.Bytes())
	p.len += 8
}

func (p *packet) AddDataFloat(data float64) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, data)
	p.data = append(p.data, buf.Bytes())
	p.len += 12
}

func (p *packet) AddDataString(data string) {
	p.data = append(p.data, []byte(data))
	p.len += uint16(len(data)) + 4
}

func (p *packet) AddData(data interface{}) {
	switch data := data.(type) {
	case int32:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, data)
		p.data = append(p.data, buf.Bytes())
		p.len += 8
	case float64:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, data)
		p.data = append(p.data, buf.Bytes())
		p.len += 12
	case string:
		p.data = append(p.data, []byte(data))
		p.len += uint16(len(data)) + 4
	case time.Time:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, int32(data.Unix()))
		p.data = append(p.data, buf.Bytes())
		p.len += 8
	default:
		log.Fatalf("Add data to packet error. Data type %t", data)
	}
}
