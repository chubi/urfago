package rpcfBlocksReportEx

import (
	"fmt"
	"time"

	"bitbucket.org/chubi/urfago"
)

type Attr struct {
	AccountId  int32
	Login      string
	StartDate  time.Time
	ExpireDate time.Time
	BlockType  int32
	Id         int32
	Unabon     int32
	Unprepay   int32
	IsDeleted  int32
}

type Account struct {
	AttrsCount int32
	Attrs      []Attr
}

type rpcfBlocksReportEx struct {
	UserId    int32
	AccountId int32
	GroupId   int32
	Apid      int32
	TimeStart time.Time
	TimeEnd   time.Time
	ShowAll   int32
}

type rpcfBlocksReportExRes struct {
	AccountsCount int32
	Accounts      []Account
}

func New() (res *rpcfBlocksReportEx) {
	res = &rpcfBlocksReportEx{}
	res.TimeEnd = time.Now()
	res.ShowAll = 1
	return
}

func (r *rpcfBlocksReportEx) Run(u *urfago.Urfa) (res *rpcfBlocksReportExRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x300b)
	if err != nil {
		return
	}
	packet := u.GetPacket()
	packet.AddData(r.UserId)
	packet.AddData(r.AccountId)
	packet.AddData(r.GroupId)
	packet.AddData(r.Apid)
	packet.AddData(r.TimeStart)
	packet.AddData(r.TimeEnd)
	packet.AddData(r.ShowAll)
	u.SendParam()

	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()
	res = &rpcfBlocksReportExRes{}
	reader.Get(&res.AccountsCount)
	for i := int32(0); i < res.AccountsCount; i++ {
		account := Account{}
		reader.Get(&account.AttrsCount)
		for j := int32(0); j < account.AttrsCount; j++ {
			attr := Attr{}
			reader.Get(&attr.AccountId)
			reader.Get(&attr.Login)
			reader.Get(&attr.StartDate)
			reader.Get(&attr.ExpireDate)
			reader.Get(&attr.BlockType)
			reader.Get(&attr.Id)
			reader.Get(&attr.Unabon)
			reader.Get(&attr.Unprepay)
			reader.Get(&attr.IsDeleted)
			account.Attrs = append(account.Attrs, attr)
		}
		res.Accounts = append(res.Accounts, account)
	}
	return
}
