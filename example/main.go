package main

import (
	"log"

	"github.com/kr/pretty"

	"bitbucket.org/chubi/urfago"
	"bitbucket.org/chubi/urfago/rpcfWhoami"
)

func main() {
	urfa, err := urfago.NewUrfa("127.0.0.1:11758", "init", "init")
	if err != nil {
		log.Fatalln(err)
	}
	for i := 0; i < 2; i++ {
		req := rpcfWhoami.NewRpcfWhoami()
		res, err := req.Run(urfa)
		if err != nil {
			log.Fatalln(err)
		}
		pretty.Log(res)
	}
}
