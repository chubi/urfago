package rpcfWhoami

import (
	"fmt"
	"net"

	"bitbucket.org/chubi/urfago"
)

type Fid struct {
	Id     int32
	Name   string
	Module string
}

type Group struct {
	Id   int32
	Name string
	Info string
}

type rpcfWhoamiRes struct {
	MyUid              int32
	Login              string
	Ip                 net.IP
	Mask               net.IP
	SystemGroupsSize   int32
	SystemGroups       []Group
	AllowedFidsSize    int32
	AllowedFids        []Fid
	NotAllowedFidsSize int32
	NotAllowedFids     []Fid
}

type rpcfWhoami struct{}

func New() (rpc *rpcfWhoami) {
	rpc = &rpcfWhoami{}
	return
}

func (r *rpcfWhoami) Run(u *urfago.Urfa) (res *rpcfWhoamiRes, err error) {
	u.Lock()
	defer u.Unlock()
	err = u.Call(0x440a)
	if err != nil {
		return
	}
	reader, err := urfago.NewReader(u)
	if err != nil {
		err = fmt.Errorf("Not data")
		return
	}
	defer reader.Clean()

	res = &rpcfWhoamiRes{}
	reader.Get(&res.MyUid)
	reader.Get(&res.Login)
	reader.Get(&res.Ip)
	reader.Get(&res.Mask)
	reader.Get(&res.SystemGroupsSize)
	for i := int32(0); i < res.SystemGroupsSize; i++ {
		group := Group{}
		reader.Get(&group.Id)
		reader.Get(&group.Name)
		reader.Get(&group.Info)
		res.SystemGroups = append(res.SystemGroups, group)
	}
	reader.Get(&res.AllowedFidsSize)
	for i := int32(0); i < res.AllowedFidsSize; i++ {
		fid := Fid{}
		reader.Get(&fid.Id)
		reader.Get(&fid.Name)
		reader.Get(&fid.Module)
		res.AllowedFids = append(res.AllowedFids, fid)
	}
	reader.Get(&res.NotAllowedFidsSize)
	for i := int32(0); i < res.NotAllowedFidsSize; i++ {
		fid := Fid{}
		reader.Get(&fid.Id)
		reader.Get(&fid.Name)
		reader.Get(&fid.Module)
		res.NotAllowedFids = append(res.NotAllowedFids, fid)
	}
	return
}
